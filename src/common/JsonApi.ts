import type { AxiosResponse } from 'axios'
import { http } from "./axios";

type ID = string | number

class JsonApi {
	constructor(public name: string) {}

	/**
	 * Get all items from json-server
	 * @param filters = ?something=test&xx=yy
	 */
	getAll<T>(filters?: string): Promise<AxiosResponse<T>> {
		return http.get<T>(`/${this.name}${filters?? ''}`)
	}

	getOne<T>(id: ID): Promise<AxiosResponse<T>> {
		return http.get<T>(`/${this.name}/${id}`)
	}

	create<T, P>(payload: P): Promise<AxiosResponse<T>> {
		return http.post<T>(`/${this.name}`, payload)
	}

	replace<T, P>(id: ID, payload: P): Promise<AxiosResponse<T>> {
		return http.put<T>(`/${this.name}/${id}`, payload)
	}

	patch<T, P>(id: ID, payload: P): Promise<AxiosResponse<T>> {
		return http.patch<T>(`/${this.name}/${id}`, payload)
	}

	delete<T>(id: ID): Promise<AxiosResponse<T>> {
		return http.delete<T>(`/${this.name}/${id}`)
	}
}

// demo how to use
// interface ICategory {
// 	name: string
// 	slug: string
// 	id: string
// 	forums: string[]
// }
// const categories = new JsonApi('categories')

// const { data } = await categories.getAll<ICategory[]>()
// data.forEach(cat => {
// 	console.log(cat.name)
// })

export default JsonApi
