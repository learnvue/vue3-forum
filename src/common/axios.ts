import axios from 'axios'

export const http = axios.create({
	baseURL: import.meta.env.VITE_APP_API_URL
})

export function setHeaderAuth(token: string) {
	http.defaults.headers.common['Authorization'] = `Bearer ${token}`
}
