import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import { authRoutes } from '@/modules/auth/router/auth.routes'
import { META_AUTH } from '@/common/constants'
import { routerAuthGuard } from '@/modules/auth/router/auth.guards'
import { forumRoutes } from '@/modules/forum/router/forum.routes'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: {
        auth: META_AUTH.EVERYONE,
      }
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue'),
      meta: {
        auth: META_AUTH.EVERYONE,
      }
    },
    ...authRoutes,
    ...forumRoutes,
  ]
})

router.beforeEach(routerAuthGuard)

export default router
