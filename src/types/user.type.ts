
export type UserData = {
	id: number
	email: string
	username: string
	isAdmin?: boolean
}

export type AuthFormData = {
	email: string
	password: string
	isAdmin?: boolean
	username?: string
}

export type AuthLoginForm = {
	email: string
	password: string
}

export type AuthRegisterForm = {
	isAdmin: boolean
	username: string
} & AuthLoginForm

export type AuthResponseData = {
	accessToken: string
	user: UserData
}
