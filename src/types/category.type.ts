
export type FullCategory = {
	id: string
	name: string
	slug: string
	forums: Forum[]
}

export type Forum = {
	id: string
	name: string
	slug: string
	categoryId: string
}

export type Topic = {
	forumId: string
	id: number,
	userId: number,
	author: string,
	title: string
	content: string
	createdAt: string
	edited: boolean,
	updatedAt: string
}
