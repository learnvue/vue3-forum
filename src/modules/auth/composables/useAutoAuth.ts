import { useAuthStore } from "../stores/auth.store";


export const useAutoAuth = () => {
	// replace by userInfos (all user infos & token)
	const userInfosJson = localStorage.getItem('userInfos')

	if (!userInfosJson) return ;
	const userInfos = JSON.parse(userInfosJson)

	const authStore = useAuthStore()
	authStore.setAuth(userInfos, true)
}
