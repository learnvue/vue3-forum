import { reactive, toRefs } from "vue"

const defaultForm = () => ({
	email: '',
	password: '',
	username: '',
	isAdmin: false
})

export function useAuthForm() {
	const form = reactive(defaultForm())
	const { email, password, username, isAdmin } = toRefs(form)

	const reset = () => {
		const {
			email: defaultEmail, password: defaultPassword,
			username: defaultUserame
		} = defaultForm()
		email.value = defaultEmail
		password.value = defaultPassword
		username.value = defaultUserame
		isAdmin.value = false
	}

	return {
		email,
		password,
		username,
		isAdmin,
		reset,
	}
}
