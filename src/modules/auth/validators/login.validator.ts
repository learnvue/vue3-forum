import { object, string, ref } from "yup"

export const loginFormValidator = () => {
	return object().shape({
		email: string().required().email(),
		password: string().min(4).max(100).required(),
	}).required()
}
