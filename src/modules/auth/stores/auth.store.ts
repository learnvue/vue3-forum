import { setHeaderAuth } from '@/common/axios'
import type { AuthResponseData, UserData } from '@/types/user.type'
import { defineStore } from 'pinia'

interface AuthState {
	accessToken: string
	user: UserData | undefined
}

export const useAuthStore = defineStore('auth', {
	state: (): AuthState => ({
		accessToken: '',
		user: undefined
	}),

	getters: {
		loggedIn(state): boolean {
			return (state.accessToken !== '')
		},
		userEmail(state): string {
			return (state.user ? state.user.email : '')
		},
		isAdmin(state): boolean {
			return (state.user?.isAdmin?? false)
		}
	},

	actions: {
		setAuth(data: AuthResponseData, fromLocal = false) {
			setHeaderAuth(data.accessToken)
			this.$patch(data)
			if (!fromLocal) {
				localStorage.setItem('userInfos', JSON.stringify(data))
			}
		}
	}
})
