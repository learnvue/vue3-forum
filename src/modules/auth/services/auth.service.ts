import { http } from "@/common/axios";
import type { AuthResponseData, AuthFormData } from "@/types/user.type";

export function postRegister(payload: AuthFormData) {
	return http.post<AuthResponseData>('/register', payload)
}

export function postLogin(payload: AuthFormData) {
	return http.post<AuthResponseData>('/login', payload)
}
