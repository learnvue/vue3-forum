import { META_AUTH } from "@/common/constants";
import type { RouteRecordRaw } from "vue-router";

export const authRoutes: RouteRecordRaw[] = [
	{
		path: '/auth',
		children: [
			{
				path: 'register',
				name: 'auth-register',
				component: () => import('../views/RegisterView.vue'),
				meta: {
					auth: META_AUTH.NO_AUTH
				}
			},
			{
				path: 'login',
				name: 'auth-login',
				component: () => import('../views/LoginView.vue'),
				meta: {
					auth: META_AUTH.NO_AUTH
				}
			}
		]
	}
]
