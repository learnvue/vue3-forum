import type { NavigationGuardNext, RouteLocation } from "vue-router";
import { useAuthStore } from "../stores/auth.store";
import { META_AUTH } from "@/common/constants";


export function routerAuthGuard(
	to: RouteLocation,
	from: RouteLocation,
	next: NavigationGuardNext
) {
	const authStore = useAuthStore()
	if (authStore.loggedIn) {
		if (to.meta.auth === META_AUTH.NO_AUTH)
			next({ name: 'home' })
		else
			next()
	}
	else {
		if (to.meta.auth === META_AUTH.AUTH)
			next({ name: 'home' })
		else
			next()
	}
}
