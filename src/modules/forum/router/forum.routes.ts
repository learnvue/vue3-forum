import { META_AUTH } from '@/common/constants';
import type { RouteRecordRaw } from 'vue-router';

export const forumRoutes: RouteRecordRaw[] = [
	{
		path: '/forum',
		name: 'forum-main',
		component: () => import('../views/MainForumView.vue'),
		children: [
			{
				path: ':categorySlug',
				name: 'forum-category',
				component: () => import('../views/CategoryView.vue'),
				meta: {
					auth: META_AUTH.EVERYONE
				}
			},
			{
				path: ':categorySlug/:forumSlug',
				name: 'forum-forum',
				component: () => import('../views/ForumView.vue'),
				meta: {
					auth: META_AUTH.EVERYONE
				}
			},
		]
	}
]
