function currentCategory(state) {
	return state.fullCategories.find(c => c.slug === state.currentCategorySlug)
}

export default currentCategory
