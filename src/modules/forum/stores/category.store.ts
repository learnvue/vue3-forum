import type { FullCategory, Topic } from "@/types/category.type";
import { defineStore } from "pinia";
import { CategoryService } from "../services/categories";
import { TopicService } from "../services/topics";
import currentCategory from "./getters/currentCategory";

interface State {
	loading_fullCategories: boolean,
	fullCategories: FullCategory[],
	currentCategorySlug: string,
	currentForumSlug: string,
	loading_topics: boolean,
	topics: Topic[]
}



export function getBy(param) {
    return state => value => {
        return state.list.find(el => el[param] == value)
    }
}

const funcArray = ['id', 'name', 'something'].map(paramName => getBy(paramName))

export const useCategoryStore = defineStore('categories', {
	state: (): State => ({
		loading_fullCategories: false,
		fullCategories: [],
		currentCategorySlug: '',
		currentForumSlug: '',
		loading_topics: false,
		topics: []
	}),

	getters: {
		currentCategory,
		currentForum(state) {
			return this.currentCategory?.forums.find(f => f.slug === this.currentForumSlug)
			// return state.fullCategories
			// 	.find(c => c.slug === state.currentCategorySlug)
			// 	?.forums.find(f => f.slug === this.currentForumSlug)
		},
	},

	actions: {
		// full categories
		async loadFullCategories() {
			this.loading_fullCategories = true

			const { data } = await CategoryService.getAll<FullCategory[]>('?_embed=forums')
			this.setFullCategories(data)

			setTimeout(async () => {
				this.loading_fullCategories = false
			}, 1000)
		},
		setFullCategories(fullCategories: FullCategory[]) {
			this.fullCategories = fullCategories
		},
		// end of full categories


		// forums
		setCurrentCategorySlug(slug: string) {
			this.currentCategorySlug = slug
		},
		setCurrentForumSlug(slug: string) {
			this.currentForumSlug = slug
		},

		async loadTopics(forumId: string | number) {
			this.loading_topics = true

			const { data } = await TopicService.getAll<Topic[]>(`?_forumId=${forumId}`)
			this.setTopics(data)

			setTimeout(async () => {
				this.loading_topics = false
			}, 1000)
		},

		setTopics(data: Topic[]) {
			this.topics = data
		}
	}
})
